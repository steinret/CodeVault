=======
README
=======
- 1. Code sample name
mklspmv

- 2. Description of the code sample package
mklspmv code performs multiplication of a sparse matrix with a dense vector using MKL's routine mkl_dcsrmv() on a multicore processor.

MKL library is required.

This is one of the code sample from the PRACE CodeVault. You can find more code samples available for download from the PRACE CodeVault here: https://gitlab.com/PRACE-4IP/CodeVault 

- 3. Release date
26 January 2016

- 4. Version history 
1.0: initial version

- 5. Contributor (s) / Maintainer(s) 
Kadir Akbudak <kadir.cs@gmail.com>

- 6. Copyright / License of the code sample
Apache 2.0

- 7. Language(s) 
C

- 8. Parallelisation Implementation(s)
Parallelism is provided by MKL.

- 9. Level of the code sample complexity 
new starts

- 10. Instructions on how to compile the code
cc=icc cmake .
make

- 11. Instructions on how to run the code
./mklspmv test.mtx 2 PRINT_YES
1. ./mklspmv : Executable
2. test.mtx  : Input matrix
3. 2         : Number of threads
4. PRINT_YES : Print input matrix to stdout

- 12. Sample input(s)
test.mtx contains a small matrix in Matrix Market format [1]

- 13. Sample output(s)
Prints timing information to output.

REFERENCES:
[1] http://math.nist.gov/MatrixMarket/formats.html
