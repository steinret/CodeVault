#include "petsc.h"
#include "petscfix.h"
/* iscoloring.c */
/* Fortran interface file */

/*
* This file was generated automatically by bfort from the C source
* file.  
 */

#ifdef PETSC_USE_POINTER_CONVERSION
#if defined(__cplusplus)
extern "C" { 
#endif 
extern void *PetscToPointer(void*);
extern int PetscFromPointer(void *);
extern void PetscRmPointer(void*);
#if defined(__cplusplus)
} 
#endif 

#else

#define PetscToPointer(a) (*(long *)(a))
#define PetscFromPointer(a) (long)(a)
#define PetscRmPointer(a)
#endif

#include "petscsys.h"
#include "petscis.h"
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define iscoloringdestroy_ ISCOLORINGDESTROY
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define iscoloringdestroy_ iscoloringdestroy
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define ispartitioningtonumbering_ ISPARTITIONINGTONUMBERING
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define ispartitioningtonumbering_ ispartitioningtonumbering
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define ispartitioningcount_ ISPARTITIONINGCOUNT
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define ispartitioningcount_ ispartitioningcount
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define isallgather_ ISALLGATHER
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define isallgather_ isallgather
#endif


/* Definitions of Fortran Wrapper routines */
#if defined(__cplusplus)
extern "C" {
#endif
void PETSC_STDCALL   iscoloringdestroy_(ISColoring iscoloring, int *__ierr ){
*__ierr = ISColoringDestroy(
	(ISColoring)PetscToPointer((iscoloring) ));
}
void PETSC_STDCALL   ispartitioningtonumbering_(IS part,IS *is, int *__ierr ){
*__ierr = ISPartitioningToNumbering(
	(IS)PetscToPointer((part) ),is);
}
void PETSC_STDCALL   ispartitioningcount_(IS part,PetscInt count[], int *__ierr ){
*__ierr = ISPartitioningCount(
	(IS)PetscToPointer((part) ),count);
}
void PETSC_STDCALL   isallgather_(IS is,IS *isout, int *__ierr ){
*__ierr = ISAllGather(
	(IS)PetscToPointer((is) ),isout);
}
#if defined(__cplusplus)
}
#endif
