=======
README
=======
The samples in this folder demonstrate the usage of PETSc (implicit parallelism) 
- Basic structures (Mat, Vec, KSP, PC, etc.) 
- Profiling  stages of the code 
- Examples can make use of different  preconditioners  and solvers (provided  via command line arguments) 

- 1. Code sample name
ksp_solver_simple.c
ksp_solver_two_sys.c
ksp_colver_multi_rhs.c

- 2. Description of the code sample package
  Samples for introduction to Krylov Subspace Methods by using PETSc:
  - Linear system  solution  in parallel 
    - 2D Laplacian (2D mesh) 
  - Repeatedly  solving  two linear systems 
    - Same preconditioner 
    - Two different matrices with the same nonzero pattern 
  - Solving  multiple  linear systems 
    - Same cofficient matrix 
    - Different right-hand-side vectors 

- 3. Release date
20 January 2016

- 4. Version history 
1.0: initial version

- 5. Contributor (s) / Maintainer(s) 
Cevdet Aykanat (aykanat@cs.bilkent.edu.tr) 
Kadir Akbudak (kadir.cs@gmail.com) 
Reha Oguz Selvitopi(reha@cs.bilkent.edu.tr) 

- 6. Copyright / License of the code sample

- 7. Language(s) 
C

- 8. Parallelisation Implementation(s)
MPI

- 9. Level of the code sample complexity 
new starters 

- 10. Instructions on how to compile the code
*Note that PETSc and MPI libraries must be available.*

$ rm -rf CMakeFiles CMakeCache.txt; cmake .;make

- 11. Instructions on how to run the code
$./run.sh

- 12. Sample input(s)
No input is required.

- 13. Sample output(s)
No file is produced. norm are printed to standard output.

