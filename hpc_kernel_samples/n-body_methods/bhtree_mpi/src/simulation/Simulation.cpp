#include <string>
#include <iostream>
#include "Simulation.hpp"

namespace nbody {
	using namespace std;

	Simulation::Simulation() {
		this->parallelRank = -1;
		this->parallelSize = -1;
		this->correctState = 0;
		this->tree = NULL;
	}

	Simulation::~Simulation() {
	}

	void Simulation::clearBodies() {
		this->bodies.clear();
	}

	void Simulation::addBodies(vector<Body> bodies) {
		this->bodies.insert(this->bodies.end(), bodies.begin(), bodies.end());
	}

	vector<Body> Simulation::getBodies() {
		return this->bodies;
	}

	bool Simulation::readInputData(string filename) {
		if (this->getProcessId() == 0) {
			this->bodies = dubinskiParse(filename);
			if (this->bodies.empty()) {
				return false;
			}
		}
		return true;
	}

	Tree* Simulation::getTree() {
		return this->tree;
	}

}
