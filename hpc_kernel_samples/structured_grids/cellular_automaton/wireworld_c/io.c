#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "io.h"

#define MAX_HEADER_LENGTH 128
#define MAX_CELLS_X (1<<15)
#define MAX_CELLS_Y (1<<15)

void file_set_view(MPI_File file, const world_t *world, size_t header_length);

void file_read_header(MPI_File file, size_t *global_size, size_t *header_length)
{
   char header[MAX_HEADER_LENGTH+1] = {0};
   char *begin, *end;
   long x, y;

   // Read header (and most likely something more)
   MPI_File_read_all(file, header, MAX_HEADER_LENGTH, MPI_CHAR, MPI_STATUS_IGNORE);

   // Parse global number of cells in x and y direction
   begin = header;
   x = strtol(begin, &end, 10);
   begin = end;
   y = strtol(begin, &end, 10);

   // Check x and y
   if(x < 0 || x > MAX_CELLS_X || y < 0 || y > MAX_CELLS_Y) {
      fprintf(stderr,
         "Global number of cells out of range (x/y): %ld / %ld\n",
         x, y
      );
      MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
   }

   // Check that header has been read to end
   if(*end != '\n') {
      *end = '\0';
      fprintf(stderr,
         "File header should end with newline character.\n"
         "Parsed portion of header: \"%s\"\n",
         header
      );
      MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
   }

   global_size[0] = x+1; // (+1 for trailing '\n' on each input file line)
   global_size[1] = y;
   *header_length = end - header + 1;
}

void file_write_header(MPI_File file, const world_t *world, size_t *header_length)
{
   int rank, len;
   char header[MAX_HEADER_LENGTH+1];

   len = snprintf(
      header, MAX_HEADER_LENGTH+1, "%ld %ld\n",
      world->global_size[0]-1, // (-1 ... do not count trailing '\n')
      world->global_size[1]
   );
   *header_length = len;

   MPI_Comm_rank(MPI_COMM_WORLD, &rank);

   if(rank == 0) {
      MPI_File_write(file, header, len, MPI_CHAR, MPI_STATUS_IGNORE);
   }
}

void file_set_view(MPI_File file, const world_t *world, size_t header_length)
{
   MPI_Datatype world_type;
   int sizes[2], subsizes[2], starts[2];
   int dim;

   for(dim = 0; dim < 2; dim++) {
      sizes[dim] = world->global_size[dim];
      subsizes[dim] = world->local_size[dim];
      starts[dim] = world->local_start[dim];
   }

   MPI_Type_create_subarray(
      2, sizes, subsizes, starts, MPI_ORDER_FORTRAN, MPI_CHAR, &world_type
   );
   MPI_Type_commit(&world_type);

   MPI_File_set_view(file, header_length, MPI_CHAR, world_type, "native", MPI_INFO_NULL);

   MPI_Type_free(&world_type);
}

void file_read_world(MPI_File file, world_t *world, size_t header_length)
{
   const size_t storage_size = world_get_storage_size(world);

   file_set_view(file, world, header_length);
   MPI_File_read_all(file, world->cells_next, 1, world->transfer.io_type, MPI_STATUS_IGNORE);
   memcpy(world->cells_prev, world->cells_next, storage_size);
}

void file_write_world(MPI_File file, const world_t *world, size_t header_length)
{
   file_set_view(file, world, header_length);
   MPI_File_write_all(file, world->cells_next, 1, world->transfer.io_type, MPI_STATUS_IGNORE);
}
