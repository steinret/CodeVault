/*
   Copyright (C) 2015  CSC - IT Center for Science Ltd.

   Licensed under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Code is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   Copy of the GNU General Public License can be onbtained from
   see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <omp.h>

int main(void)
{
    int tid, nthreads;

#pragma omp parallel private(tid) shared(nthreads)
    {
#pragma omp single
        {
            nthreads = omp_get_num_threads();
            printf("There are %i threads in total.\n", nthreads);
        }

        tid = omp_get_thread_num();

#pragma omp critical
        printf("Hello from thread id %i/%i!\n", tid, nthreads);
    }

    return 0;
}
